# termux-photo-viewer

Take photos on Android devices using Termux and view them in your browser.

## Termux setup on the phone

1. Install Termux (https://termux.dev/en/) and Termux API on a compatible
Android device.

2. Install openssh

	a. `apk install openssh`
	
	b. set up password (https://wiki.termux.com/wiki/Remote_Access#Using_the_SSH_server)
	
	c. run `sshd`
	
	d. obtain a wake lock in the Termux notification item

3. The device is now ready to receive ssh connections

## Running the image

The image can only be run on a host that has SSH keys needed to connect to the
Android device. Test this by connecting to the device from the shell before
starting the Docker container by executing:

```shell
ssh -p <TERMUX_SSH_PORT> <TERMUX_USER>@<TERMUX_IP>
```

Replace the SSH port, user and IP with the correct values.

If you successfully connected to the Android device, you can proceed to running
the Docker container:

```shell
    docker run -d \
        -p 5000:5000 \
        --restart unless-stopped \
        --name termux-photo-viewer-cont \
        -v /home/user/.ssh:/app/.ssh \
        -e TPV_PORT='TERMUX_SSH_PORT' \
        -e TPV_USER='TERMUX_USER' \
        -e TPV_HOST='TERMUX_IP' \
        pumpkinsteel/termux-photo-viewer:arm64
```

**IMPORTANT**: If you are running the container on a Raspberry Pi, you should
use the `arm64` tag. If you are running it on an X86 device, use the `amd64`
tag.

## Viewing the photos in the browser

Visit `http://localhost:5000/` in your browser.